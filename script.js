let map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]
// transformar celulas em div //
let divMap = document.querySelector(".map")
let positionX = ""
let positionY = ""
let actualPosition = ""
const config = {
    generatingMap: function () {
        for (let i = 0; i < map.length; i++) {
            const arrayLine = document.createElement("div")
            arrayLine.style.display = "flex"
            for (let j = 0; j < map[i].length; j++) {
                cell = document.createElement("div")
                cell.classList.add("path")
                arrayLine.appendChild(cell)
                cell.id = `div${i}-${j}`
                if (map[i][j] === "W") {
                    cell.classList.add("wall")
                }
                if (map[i][j] === "S") {
                    const player = document.createElement("div")
                    player.classList.add("player")
                    cell.appendChild(player)
                }
            }
            divMap.appendChild(arrayLine)
        }
    },
    tracking: function () {
        for (let i = 0; i < map.length; i++) {
            for (let j = 0; j < map[i].length; j++) {
                if (map[i][j] === "S") {
                    positionX = i
                    positionY = j
                }
            }

        }
    },
}
const moves = {
    "ArrowDown": function () {
        if (map[positionX + 1][positionY] !== "W") {
            positionX += 1
        }
    },
    "ArrowUp": function () {
        if (map[positionX - 1][positionY] !== "W") {
            positionX -= 1
        }
    },
    "ArrowRight": function () {
        if (map[positionX][positionY + 1] !== "W") {
            positionY += 1
        }
    },
    "ArrowLeft": function () {
        if (map[positionX][positionY - 1] !== "W") {
            positionY -= 1
        }
    }
}

let append = () => {
    actualPosition = document.querySelector(`#div${positionX}-${positionY}`)
    actualPosition.appendChild(document.querySelector(".player"))
     winCheck()
}

const winCheck = () => {
    if (map[positionX][positionY] === "F") {
        document.querySelector(".victory-img").style.visibility = "visible"
    }
}

config.generatingMap()
config.tracking()
window.addEventListener("keydown", function (event) {
    moves[event.key]()
    append()
})